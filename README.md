# How to Deploy

### Create NameSpace

Execute file kub_deploy/namespace.yml

```kubectl apply -f kub_deploy/namespace.yml```

### Create Redis Instance

Execute file kub_deploy/redis.yml

```kubectl apply -f kub_deploy/redis.yml```

### Create NGINX Ingress Controller

Execute command

```kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/aws/deploy.yaml```

Verify pod progress

```kubectl get pods -n ingress-nginx -l app.kubernetes.io/name=ingress-nginx --watch```

### Install Prometheus

Execute command to deploy prometheus

```
helm repo add stable https://charts.helm.sh/stable
helm repo update

helm install -name prom -n monitor stable/prometheus-operator
```

Execute this file to setup ingress for Prometheus

```kubectl apply -f kub_deploy/promingress.yml```

Verify pod progress

```kubectl get pods -n monitor```

Try accessing Prometheus with http://prom.monitor.local
* add 192.X.X.X prom.monitor.local to /etc/hosts file

### Grafana install

Execute command to add repo

```
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update  

helm install -name grafana -n monitor grafana/grafana
```

Execute this file to setup ingress for Grafana

```kubectl apply -f kub_deploy/grafanaingress.yml```

Try accessing Grafana with http://grafana.monitor.local
* add 192.X.X.X grafana.monitor.local to /etc/hosts file

### Install Jaeger

Execute the following command
```
kubectl create -n monitor -f https://raw.githubusercontent.com/jaegertracing/jaeger-operator/master/deploy/crds/jaegertracing.io_jaegers_crd.yaml
kubectl create -n monitor -f https://raw.githubusercontent.com/jaegertracing/jaeger-operator/master/deploy/service_account.yaml
kubectl create -n monitor -f https://raw.githubusercontent.com/jaegertracing/jaeger-operator/master/deploy/role.yaml
kubectl create -n monitor -f https://raw.githubusercontent.com/jaegertracing/jaeger-operator/master/deploy/role_binding.yaml
kubectl create -n monitor -f https://raw.githubusercontent.com/jaegertracing/jaeger-operator/master/deploy/operator.yaml
```

Execute this file to deploy jaeger

```
kubectl apply -f kub_deploy/jaeger_new.yml
```

Execute this file to setup ingress for jaeger

```
kubectl apply -f kub_deploy/jaegeringress.yml
```

Verify pod progress

```
kubectl get pods -n monitor
```

Try accessing jaeger with http://jaeger.monitor.local
* add 192.X.X.X jaeger.monitor.local to /etc/hosts file

### Create Keycloak instance

Execute command to setup keycloak

```
kubectl apply -f kub_deploy/keycloak.yml
```

Execute command to setup Keycloak Ingress controller

```
kubectl apply -f kub_deploy/keycloakingress.yml
```

Note :- After Keycload is up and Running create realm and configure client
* add 192.X.X.X keycloak to /etc/hosts file
* ugly non production solution, need fix it

### Create GateWay Instance

Execute command to create gateway instance

```
kubectl apply -f kub_deploy/gateway.yml
```

Execute command to create gateway ingress

```
kubectl apply -f kub_deploy/gatewayingress.yml
```

How Prometheus know where to scrap prometheus end, we need to create service monitor

Execute command to create gateway service monitor

```kubectl apply -f gatewayservicemonitor.yml```

Check the target in Prometheus

# Create Micoservices instance

Create config map based on environment and we don't have to change code on different envoronments

```kubectl apply -f ms1configmap.yml```

Run command to create microservice1 instance

```kubectl apply -f micro1.yml```

Run command to create microservice2 instance

```kubectl apply -f micro2.yml```

Run command to create microservice3 instance

```kubectl apply -f micro3.yml```

Create service Monitor for services micro1, micro2 and micro3

```
kubectl apply -f micro1servicemonitor.yml
kubectl apply -f micro2servicemonitor.yml
kubectl apply -f micro3servicemonitor.yml
```

### Install Loki

Launch command to install Loki

```
helm repo add loki https://grafana.github.io/loki/charts
helm repo update
helm install loki-stack loki/loki-stack --namespace monitor --set promtail.enabled=true,loki.persistence.enabled=true,loki.persistence.size=1Gi
```