# Micro Service 1 (micro1-app)

- Webflux is used
- Resilience4j library is used
- Circuit Breaker, Bulkahead, Ratelimiter, Timelimiter, Retry and aggregate Pattern are implemented
- Jaeger is configured for distributed tracing
- Prometheus endpoint is also enabled
- Keycload is also enabled for token validation

#### Circuit Breaker config

```
resilience4j:
  retry:
    instances:
      retryService:
        max-retry-attempts: 3
        wait-duration: 5s
        enable-exponential-backoff: true
        exponential-backoff-multiplier: 2
        
  circuitbreaker:
    instances:
      mainService:
        minimum-number-of-calls: 5
        permitted-number-of-calls-in-half-open-state: 2
        wait-duration-in-open-state: 10s
        failure-rate-threshold: 50
        event-consumer-buffer-size: 10
        automatic-transition-from-open-to-half-open-enabled: true
        register-health-indicator: true
        sliding-window-size: 10
        sliding-window-type: COUNT_BASED
    circuit-breaker-aspect-order: 2
```

#### Rate Limiter config
100 calls are allowed in 5 sec

```
ratelimiter:
  instances:
    ratelimiterservice:
      limit-for-period: 100
      limit-refresh-period: 5s
      timeout-duration: 0
      allow-health-indicator-to-fail: true
      register-health-indicator: true        
  rate-limiter-aspect-order: 1
```

#### TimeLimiter config

```
timelimiter:
  instances:
    timelimiterservice:
      timeout-duration: 5s
      cancel-running-future: true 
```

#### Bulkhead config

```
bulkhead:
  instances:
    bulkaheadservice:
      max-concurrent-calls: 30
      max-wait-duration: 0    
  metrics:
    enabled: true
```

#### Retry config

```
resilience4j:
  retry:
    instances:
      retryService:
        max-retry-attempts: 3
        wait-duration: 5s
        enable-exponential-backoff: true
        exponential-backoff-multiplier: 2
```

#### Spring Micrometer with Prometheus

```
@Timed(value = "TestController.checkrequest",description = "checkrequest" ,histogram = true)
public Mono<String> checkrequest() {
```

Config Beans

```
@Bean
MeterRegistryCustomizer meterRegistryCustomizer(MeterRegistry meterRegistry) {
  return meterRegistry1 -> {
    meterRegistry.config()
        .commonTags("application", "micrometer-monitoring-example");
  };
}

@Bean
TimedAspect timedAspect(MeterRegistry reg) {
  return new TimedAspect(reg);
}
```

#### Distributed Tracking with Jaeger

```
opentracing:
  jaeger:
    enabled: true
    service-name: ms1-app-server
    udp-sender:
      host: simplest-agent
      port: 6831 
    enable-b3-propagation: true
    log-spans: true
    const-sampler:
      decision: true
```

#### Keycloak with Spring Boot

Dependencies

```
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-security</artifactId>
</dependency>
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-oauth2-client</artifactId>
</dependency>
<dependency>
  <groupId>org.keycloak</groupId>
  <artifactId>keycloak-spring-security-adapter</artifactId>
  <version>${keycloak.adapter.version}</version>
</dependency>
```

Config application.yml

```
keycloak:
  auth-server-url: http://keycloak:8080/auth/
  resource: spring-cloud-test
  credentials:
    secret : 5d301d6d-99f2-4995-bb12-2405dd98d669
  use-resource-role-mappings : true
  principal-attribute: preferred_username
  bearer-only: true
  realm: cloud
```

Config Bean

```
@Bean
public KeycloakSpringBootConfigResolver keycloakSpringBootConfigResolver()
{
    return new KeycloakSpringBootConfigResolver();
}
```

SecurityConfig.class for keycloak and role mapping