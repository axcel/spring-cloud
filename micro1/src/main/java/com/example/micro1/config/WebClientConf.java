package com.example.micro1.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConf {
    @Value("${ms2.serviceurl}")
    private String ms2Url;

    @Value("${ms2.serviceurl.port}")
    private String ms2Port;

    @Value("${ms3.serviceurl}")
    private String ms3Url;

    @Value("${ms3.serviceurl.port}")
    private String ms3Port;

    @Bean
    public WebClient ms2WebClient() {
        System.out.println(ms2Url);
        return WebClient.create("http://"+ms2Url+":"+ms2Port);
    }

    @Bean
    public WebClient ms3WebClient() {
        return WebClient.create("http://"+ms3Url+":"+ms3Port);
    }
}
