package com.example.micro1.service;

import org.springframework.stereotype.Service;

@Service
public class TestService {
    public String ping() {
        return "PING";
    }
}
