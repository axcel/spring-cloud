package com.example.micro1.controller;

import com.example.micro1.service.TestService;
import io.github.resilience4j.bulkhead.BulkheadFullException;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import reactor.util.function.Tuple2;

@RestController
@RequestMapping("/ms1")
public class TestController {

    @Autowired
    WebClient ms2WebClient;

    @Autowired
    WebClient ms3WebClient;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    TestService testService;

    @GetMapping("/me")
    public Object getMe() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    @GetMapping(value = "/getdata")
    public Mono<String> getData() {
        System.out.println("Inside MicroService1 getData method");

        Mono<String> data = Mono.just("Hello from Reactive MicroService1 getData method!!");
        return data;
    }

    @GetMapping(value = "/checkrequest")
    @CircuitBreaker(name = "mainService", fallbackMethod = "fallback")
    @RateLimiter(name = "ratelimiterservice", fallbackMethod = "fallback")
    @TimeLimiter(name = "timelimitersercice", fallbackMethod = "fallback")
    @Bulkhead(name = "bulkaheadservice", fallbackMethod = "fallback")
    @Retry(name = "retryService")
    @Timed(value = "TestController.checkrequest", description = "checkrequest", histogram = true)
    public Mono<String> checkrequest() {
        System.out.println("inside checkrequest");

        Mono<String> mono2 = ms2WebClient.get().uri("/checkrequestcall").retrieve().bodyToMono(String.class);
        Mono<String> mono3 = ms3WebClient.get().uri("/checkrequestcall").retrieve().bodyToMono(String.class);

        return Mono.zip(mono2, mono3)
            .map(tuple -> tuple.getT1() + " " + tuple.getT2());
    }

    @GetMapping(value = "/checkrequestms2")
    @Timed(value = "TestController.checkrequestms2", description = "checkrequestMS2", histogram = true)
    public Mono<String> checkrequestMS2() {
        System.out.println("inside checkrequest MS2");

        return ms2WebClient.get().uri("/checkrequestcall").retrieve().bodyToMono(String.class);

    }

    @GetMapping(value = "/checkrequestms3")
//    @CircuitBreaker(name = "mainService", fallbackMethod = "fallback")
//    @RateLimiter(name = "ratelimiterservice", fallbackMethod = "fallback")
//    @TimeLimiter(name = "timelimitersercice", fallbackMethod = "fallback")
//    @Bulkhead(name = "bulkaheadservice", fallbackMethod = "fallback")
//    @Retry(name = "retryService")
    @Timed(value = "TestController.checkrequestms3", description = "checkrequestMS3", histogram = true)
    public Mono<String> checkrequestMS3() {
        System.out.println("inside checkrequest MS3");

        return ms3WebClient.get().uri("/checkrequestcall").retrieve().bodyToMono(String.class);

    }

    @GetMapping("/ping")
    @Timed(value = "TestController.ping", description = "ping", histogram = true)
    public String ping() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return testService.ping();
    }

    @GetMapping("/admin")
    @Timed(value = "TestController.admin", description = "admin", histogram = true)
    public String pingadmin() {
        return "admin";
    }


    public Mono<String> fallback(BulkheadFullException ex) {
        System.out.println("---------------------------------------------BulkheadFullException-----------------------------------");
        Mono<String> data = Mono.just("BulkheadFullException");
        return data;
    }

    public Mono<String> fallback(CallNotPermittedException ex) {
        System.out.println("---------------------------------------------CB OPEN-----------------------------------");
        Mono<String> data = Mono.just("CB OPEN");
        return data;
    }

    public Mono<String> fallback(HttpServerErrorException ex) {
        System.out
            .println("-------------------------------------------- Exception -----------------------------------");
        Mono<String> data = Mono.just("Exception");
        return data;
    }


    public Mono<String> fallback(RequestNotPermitted ex) {
        System.out.println(
            "-------------------------------------------- Rate Limited -----------------------------------");
        Mono<String> data = Mono.just("Rate Limited");
        return data;
    }

    public Mono<String> fallbackcb(RuntimeException ex) {
        System.out.println(
            "---------------------------------------------FALLBACK1 cb-----------------------------------");
        Mono<String> data = Mono.just("hello ms1 CB fallback");
        return data;
    }

    public Mono<String> fallbackcb(Throwable ex) {
        System.out.println(
            "---------------------------------------------FALLBACK 2 cb-----------------------------------");
        Mono<String> data = Mono.just("hello ms1 CB fallback");
        return data;
    }

    public Mono<String> fallbackretry(Throwable ex) {
        System.out.println(
            "---------------------------------------------FALLBACK RETRY-----------------------------------");
        Mono<String> data = Mono.just("RETRY");
        return data;
    }
}
